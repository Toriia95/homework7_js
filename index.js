function filterBy(arr, type) {
    return arr.filter(item => {
        if (type === 'null') {
            return item !== null;
        }
        if (type === 'object') {
            return typeof item !== type || item === null;
        }
        return typeof item !== type;
    });
}
